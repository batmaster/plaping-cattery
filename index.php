<?php include 'db_connection.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
    <script src="js/tether.min.js"></script>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.PrintArea.js"></script>

    <meta charset="UTF-8">
    <title>ชมรมแมวแห่งประเทศไทย</title>
    <style>
    body {
        margin-top: 20px;
        margin-bottom: 50px;
        background-color: #fbfbfb;
    }
    .container {
        border: solid #f7f7f9;
        background-color: #ffffff;
        padding: 30px;
    }
    .col-centered {
        float: none;
        margin: 0 auto;
    }
    .hidden {
        display: none;
    }
    </style>
</head>
<body>
    <div class="container">
        <?php
        if (isset($_GET['page'])) {
            if ($_GET['page'] == 'member') {
                include 'regis-member.php';
            }
            else if ($_GET['page'] == 'cattery') {
                include 'regis-cattery.php';
            }
            else if ($_GET['page'] == 'accept') {
                include 'accept.php';
            }
        }
        else {
            echo "
            <div class='jumbotron'>
            <form method='GET' action='index.php'>
            <button class='btn btn-primary btn-lg btn-block' type='submit' name='page' value='member'>ลงทะเบียนสมาชิก</button>
            <br>
            <button class='btn btn-primary btn-lg btn-block' type='submit' name='page' value='cattery'>ลงทะเบียนฟาร์ม</button>
            </form>
            </div>
            ";
        }

        ?>


    </div>
</body>
</html>
