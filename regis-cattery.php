<form method="POST" action="?page=accept">
    <div class="col-xs-9 col-centered row">
        <div class="col-xs-12" id="printArea">
            <center><h3>TCC MEMBER REGISTRATION APPLICATION</h3></center>
            <center><h3>ลงทะเบียนฟาร์ม</h3></center>
            <br>

            <div class="row">
                <div class="col-xs-6 hidden">
                    <label for="number">Cattery No.</label>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">C-</div>
                            <input type="text" class="form-control" name="number" id="number">
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 hidden">
                    <label for="receipt">ใบเสร็จเลขที่</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="receipt" id="receipt">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <label for="date">วันที่ลงทะเบียน</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="date" id="date" value="<?php echo (new DateTime())->format('Y-m-d H:i:s');?>" readonly>
                    </div>
                </div>
                <div class="col-xs-6 hidden">
                    <label for="expiration">หมดอายุ</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="expiration" id="expiration">
                    </div>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-xs-4">
                    <label for="name_th">ชื่อ-นามสกุล (ไทย)</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="name_th" id="name_th" required>
                    </div>
                </div>
                <div class="col-xs-4">
                    <label for="name_en">ชื่อ-นามสกุล (​ENG)</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="name_en" id="name_en" style="text-transform:uppercase" pattern="[a-zA-Z][a-zA-Z0-9\s]*" required>
                    </div>
                </div>
                <div class="col-xs-4">
                    <label for="member_number">เลขทะเบียนสมาชิก</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="member_number" id="member_number" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <label for="phone">เบอร์มือถือ</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="phone" id="phone" pattern="[0-9]{10}" required>
                    </div>
                </div>
                <div class="col-xs-6">
                    <label for="web_fb">เว็บไซต์หรือเฟสบุ๊ค</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="web_fb" id="web_fb" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <label for="email">อีเมล์</label>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" id="email" required>
                    </div>
                </div>
                <div class="col-xs-6">
                    <label for="line">Line ID</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="line" id="line" required>
                    </div>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-xs-12">
                    <label for="note">หมายเหตุ</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="note" id="note">
                    </div>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-xs-12">
                    <label for="farm_name">ชื่อฟาร์มไม่เกิน 12 ตัวอักษร เครื่องหมายต่างๆ นับเป็นหนึ่งตัวอักษร รวมถึงเว้นวรรคด้วย</label>
                    <div class="form-group" id="farm_name_group">
                        <input type="text" class="form-control form-control-lg" name="farm_name" id="farm_name" maxlength="12" required>
                    </div>
                    <p id="warning" class="hidden" align="right" style="color: #d9534f;">ชื่อฟาร์มนี้ มีผู้ใช้แล้ว</p>
                </div>
            </div>
            <br>
            <br>

            <div class="jumbotron">
                <center><h3>ข้าพเจ้ายินยอมและปฏิบัติตามกฎระเบียบข้อบังคับของ ชมรมแมวแห่งประเทศไทย</h3></center>
                <p>
                    <div class="checkbox">
                        500 บาท/ปี
                        <label>
                            <input type="checkbox" required>
                            เงินสด
                        </label>
                    </div>
                    <div class="checkbox">
                        500 บาท/ปี
                        <label>
                            <input type="checkbox" required>
                            โอนเข้าบัญชี นางสุนันท์ โซวประเสริฐสุข กสิกรไทย 261-2-13432-7 พุทธมณฑลสาย 4
                        </label>
                    </div>
                </p>
                <center><h5>สามารถตรวจสอบรายชื่อฟาร์มได้ที่เว็บ www.cfct-cat.com เมนูดาวน์โหลดแบบฟอร์ม</h5></center>
            </div>
            <input type="hidden" name="form" value="cattery">
            <center>
                <button class="btn btn-primary" type="submit" id="submit">ลงทะเบียน</button>
                <a class='btn btn-primary' href='index.php' role='button'>กลับหน้าหลัก</a>
            </center>
        </div>
    </div>
</form>

<script type="text/javascript">

$(document).ready(function() {
    $(':input').each(function () {
        var alert;
        if ($(this).attr("id") == "phone") {
            alert = "ตัวเลข 10 หลัก";
        }
        else if ($(this).attr("id") == "name_en") {
            alert = "ตัวอักษรภาษาอังกฤษเท่านั้น";
        }
        else {
            alert = "โปรดกรอกข้อมูล";
        }

        this.oninvalid = function(e) {
            if (!e.target.validity.valid) {
                e.target.setCustomValidity(alert);
            }
            else {
                e.target.setCustomValidity("");
            }
        };
        this.onchange = function(e) {
            e.target.setCustomValidity("");
        }
    });
});

$("#farm_name").keyup(function() {
    $.ajax({
        url: 'forajax.php',
        type: "POST",
        // dataType: "json",
        data: {
            "function": "check_farm_name",
            "farm_name": $("#farm_name").val()
        }
    }).done(function(response) {
        console.log(response);
        if (response == 0) {
            $("#farm_name_group").removeClass("has-error");
            $("#farm_name").removeClass("form-control-danger");
            $("#warning").addClass("hidden");
            $("#submit").prop('disabled', false);
        }
        else {
            $("#farm_name_group").addClass("has-error");
            $("#farm_name").addClass("form-control-danger");
            $("#warning").removeClass("hidden");
            $("#submit").prop('disabled', true);
        }
        // window.location = "?page=profile"
    });
});

</script>
