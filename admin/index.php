<?php ob_start(); ?>
<?php include '../db_connection.php'; ?>

<?php
$cookie_name = "username";

if (isset($_POST["username"])) {
    $username = $_POST["username"];
    $password = $_POST["password"];

    if ($username == "admin" && $password == "adminpass") {
        setcookie($cookie_name, $username, time() + (86400 * 30), "/");
        header("Refresh:0");
    }
}
else if (isset($_POST["logout"])) {
    unset($_COOKIE[$cookie_name]);
    setcookie($cookie_name, null, time() - (86400 * 30), "/");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/dashboard.css">

    <meta charset="UTF-8">
    <title>Admin - ชมรมแมวแห่งประเทศไทย</title>
    <style>
        .col-centered {
            float: none;
            margin: 0 auto;
        }

        .table thead th {
            text-align: center;
        }

        .table-success thead th {
            border-color: #d6e9c6;
            color: #3c763d;
            background-color: #dff0d8;
        }
        .table-success thead td {
            border-color: #d6e9c6;
        }

        .table-info thead th {
            border-color: #bce8f1;
            color: #31708f;;
            background-color: #d9edf7;
        }
        .table-info thead td {
            border-color: #bce8f1;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">ชมรมแมวแห่งประเทศไทย</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <form name="logout" method="POST" action="?page=dashboard">
                        <input type="hidden" name="logout">
                    </form>
                    <li <?php if (!isset($_COOKIE[$cookie_name])) echo "style='display:none'" ?>><a href="#" onclick="document.forms['logout'].submit(); return false;">Logout</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <?php
                    $page = "dashboard";
                    if ($_GET["page"] == "member") {
                        $page = "member";
                    }
                    else if ($_GET["page"] == "cattery") {
                        $page = "cattery";
                    }
                    ?>
                    <li <?php if ($page == "dashboard") echo "class='active'";?>><a href="?page=dashboard">ภาพรวม <span class="sr-only">(current)</span></a></li>
                    <li <?php if ($page == "member") echo "class='active'";?>><a href="?page=member">รายชื่อสมาชิก</a></li>
                    <li <?php if ($page == "cattery") echo "class='active'";?>><a href="?page=cattery">รายชื่อฟาร์ม</a></li>
                </ul>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <?php
                if(!isset($_COOKIE[$cookie_name])) {
                    include 'login.php';
                }
                else {
                    setcookie($cookie_name, $_COOKIE[$cookie_name], time() + (86400 * 30), "/");

                    if (isset($_GET['page'])) {
                        if ($_GET['page'] == 'member') {
                            include 'member.php';
                        }
                        else if ($_GET['page'] == 'cattery') {
                            include 'cattery.php';
                        }
                        else {
                            include 'dashboard.php';
                        }
                    }
                    else {
                        include 'dashboard.php';
                    }
                }

                ?>
            </div>
        </div>
    </div>

</body>
</html>

<?php ob_end_flush(); ?>
