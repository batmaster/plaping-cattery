<div class="col-xs-4 col-centered">
    <form class="form-signin" method="POST" action="<?php echo "?" .$_SERVER['QUERY_STRING']; ?>">
        <h2 class="form-signin-heading">Please sign in</h2>

        <label for="inputEmail" class="sr-only">Username</label>
        <input type="text" name="username" class="form-control" placeholder="Username" autofocus="" required>

        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" class="form-control" placeholder="Password" required>

        <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">ล็อคอิน</button>
    </form>

</div>
