<div class="panel panel-default">
    <!-- Default panel contents -->

    <!-- Table -->
    <table class="table table-bordered table-hover table-success">
        <thead>
            <tr>
                <th>#</th>
                <th><a href="?page=member&order=<?php if ($_GET["order"] == "asc") echo "desc"; else echo "asc" ?>">วันที่ลงทะเบียน <span class="glyphicon <?php if ($_GET["order"] == "asc") echo "glyphicon-sort-by-attributes"; else echo "glyphicon glyphicon-sort-by-attributes-alt"; ?>"></span></th>
                <th>ชื่อ-นามสกุล</th>
                <th>เลขบัตรประชาชน</th>
                <th>ที่อยู่</th>
                <th>เบอร์มือถือ</th>
                <th>เว็บไซต์หรือเฟสบุ๊ค</th>
                <th>อีเมล์</th>
                <th>Line ID</th>
                <th>หมายเหตุ</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $result = sql("SELECT * FROM member ORDER BY id " .($_GET["order"] == "asc" ? "ASC" : "DESC"));
                foreach ($result as $r) {
                    $i = $r["i"];
                    $id = $r["id"];
                    $date = $r["date"];
                    $name_th = $r["name_th"];
                    $name_en = $r["name_en"];
                    $card_id = $r["card_id"];
                    $address_th = $r["address_th"];
                    $address_en = $r["address_en"];
                    $phone = $r["phone"];
                    $web_fb = $r["web_fb"];
                    $email = $r["email"];
                    $line = $r["line"];
                    $note = $r["note"];

                    $url = strtok($_SERVER['REQUEST_URI'], '?') ."print_member.php";

                    echo "
                    <tr>
                        <form target='_blank' name='print$i' method='POST' action='$url'><input type='hidden' name='id' value='$id'></form>
                        <td>$i<br><a href='#' onclick='document.forms[\"print$i\"].submit(); return false;'><span class='glyphicon glyphicon-print'></span></a></td>
                        <td>$date</td>
                        <td><b>$name_th</b><br><i>$name_en</i></td>
                        <td>$card_id</td>
                        <td><b>$address_th</b><br><i>$address_en</i></td>
                        <td>$phone</td>
                        <td>$web_fb</td>
                        <td>$email</td>
                        <td>$line</td>
                        <td>$note</td>
                    </tr>
                    ";
                }
            ?>

        </tbody>
    </table>
</div>
