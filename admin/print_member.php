<script src="../js/tether.min.js"></script>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.PrintArea.js"></script>
<meta charset="UTF-8">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
<style>
body {
    margin-top: 20px;
    margin-bottom: 50px;
    background-color: #fbfbfb;
}
.container {
    border: solid #f7f7f9;
    background-color: #ffffff;
    padding: 30px;
}
.col-centered {
    float: none;
    margin: 0 auto;
}
.hidden {
    display: none;
}
</style>

<?php
    include '../db_connection.php';

    if (!isset($_POST["id"])) {
        echo "<script>window.close();</script>";
    }

    $sql = "SELECT * FROM member WHERE id = " .$_POST["id"];
    $r = sql($sql);
    $number = $r["number"];
    $receipt = $r["receipt"];
    $date = $r["date"];
    $expiration = $r["expiration"];
    $name_th = $r["name_th"];
    $name_en = $r["name_en"];
    $card_id = $r["card_id"];
    $address_th = $r["address_th"];
    $address_en = $r["address_en"];
    $phone = $r["phone"];
    $web_fb = $r["web_fb"];
    $email = $r["email"];
    $line = $r["line"];
    $note = $r["note"];
?>

<div class="col-xs-9 col-centered row">
    <div class="col-xs-12" id="printArea">
        <center><h3>TCC MEMBER REGISTRATION APPLICATION</h3></center>
        <center><h3>ลงทะเบียนสมาชิก</h3></center>
        <br>

        <div class="row">
            <div class="col-xs-6 hidden">
                <label for="number">Cattery No.</label>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" value="<?php echo $number; ?>" readonly>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 hidden">
                <label for="receipt">ใบเสร็จเลขที่</label>
                <div class="form-group">
                    <input type="text" class="form-control" value="<?php echo $receipt; ?>" readonly>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                <label for="date">วันที่ลงทะเบียน</label>
                <div class="form-group">
                    <input type="text" class="form-control" value="<?php echo $date; ?>" readonly>
                </div>
            </div>
            <div class="col-xs-6 hidden">
                <label for="expiration">หมดอายุ</label>
                <div class="form-group">
                    <input type="text" class="form-control" value="<?php echo $expiration; ?>" readonly>
                </div>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-4">
                <label for="name_th">ชื่อ-นามสกุล (ไทย)</label>
                <div class="form-group">
                    <input type="text" class="form-control"value="<?php echo $name_th; ?>" readonly>
                </div>
            </div>
            <div class="col-xs-4">
                <label for="name_en">ชื่อ-นามสกุล (​ENG)</label>
                <div class="form-group">
                    <input type="text" class="form-control" style="text-transform:uppercase" value="<?php echo $name_en; ?>" readonly>
                </div>
            </div>
            <div class="col-xs-4">
                <label for="card_id">เลขบัตรประชาชน (CARD ID)</label>
                <div class="form-group">
                    <input type="text" class="form-control" pattern="[0-9]{13}" value="<?php echo $card_id; ?>" readonly>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <label for="address_th">ที่อยู่ (ไทย)</label>
                <div class="form-group">
                    <textarea class="form-control" rows="3" readonly><?php echo $address_th; ?></textarea>
                </div>
            </div>
            <div class="col-xs-12">
                <label for="address_en">ที่อยู่ (ENG)</label>
                <div class="form-group">
                    <textarea class="form-control" rows="3" readonly><?php echo $address_en; ?></textarea>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                <label for="phone">เบอร์มือถือ</label>
                <div class="form-group">
                    <input type="text" class="form-control" pattern="[0-9]{10}" value="<?php echo $phone; ?>" readonly>
                </div>
            </div>
            <div class="col-xs-6">
                <label for="web_fb">เว็บไซต์หรือเฟสบุ๊ค</label>
                <div class="form-group">
                    <input type="text" class="form-control" value="<?php echo $web_fb; ?>" readonly>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
                <label for="email">อีเมล์</label>
                <div class="form-group">
                    <input type="email" class="form-control" value="<?php echo $email; ?>" readonly>
                </div>
            </div>
            <div class="col-xs-6">
                <label for="line">Line ID</label>
                <div class="form-group">
                    <input type="text" class="form-control" value="<?php echo $line; ?>" readonly>
                </div>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-xs-12">
                <label for="note">หมายเหตุ</label>
                <div class="form-group">
                    <input type="text" class="form-control" value="<?php echo $note; ?>" readonly>
                </div>
            </div>
        </div>
        <br>
        <br>
    </div>

    <button class="btn btn-primary btn-lg btn-block" onclick="$('#printArea').printArea();">พิมพ์</button>
</div>

<script type="text/javascript">
    $("#printArea").printArea();

    jQuery(document).bind("keyup keydown", function(e) {
        e.preventDefault();
    });

</script>
