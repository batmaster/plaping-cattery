<form method="POST" action="?page=accept">
    <div class="col-xs-9 col-centered row">
        <div class="col-xs-12" id="printArea">
            <center><h3>TCC MEMBER REGISTRATION APPLICATION</h3></center>
            <center><h3>ลงทะเบียนสมาชิก</h3></center>
            <br>

            <div class="row">
                <div class="col-xs-6 hidden">
                    <label for="number">Cattery No.</label>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">M-</div>
                            <input type="text" class="form-control" name="number" id="number">
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 hidden">
                    <label for="receipt">ใบเสร็จเลขที่</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="receipt" id="receipt">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <label for="date">วันที่ลงทะเบียน</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="date" id="date" value="<?php echo (new DateTime())->format('Y-m-d H:i:s');?>" readonly>
                    </div>
                </div>
                <div class="col-xs-6 hidden">
                    <label for="expiration">หมดอายุ</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="expiration" id="expiration">
                    </div>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-xs-4">
                    <label for="name_th">ชื่อ-นามสกุล (ไทย)</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="name_th" id="name_th" required>
                    </div>
                </div>
                <div class="col-xs-4">
                    <label for="name_en">ชื่อ-นามสกุล (​ENG)</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="name_en" id="name_en" style="text-transform:uppercase" pattern="[a-zA-Z][a-zA-Z0-9\s]*" required>
                    </div>
                </div>
                <div class="col-xs-4">
                    <label for="card_id">เลขบัตรประชาชน (CARD ID)</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="card_id" id="card_id" pattern="[0-9]{13}" maxlength="13" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <label for="address_th">ที่อยู่ (ไทย)</label>
                    <div class="form-group">
                        <textarea class="form-control" rows="3" name="address_th" id="address_th" required></textarea>
                    </div>
                </div>
                <div class="col-xs-12">
                    <label for="address_en">ที่อยู่ (ENG)</label>
                    <div class="form-group">
                        <textarea class="form-control" rows="3" name="address_en" id="address_en" required></textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <label for="phone">เบอร์มือถือ</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="phone" id="phone" pattern="[0-9]{10}" required>
                    </div>
                </div>
                <div class="col-xs-6">
                    <label for="web_fb">เว็บไซต์หรือเฟสบุ๊ค</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="web_fb" id="web_fb" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <label for="email">อีเมล์</label>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" id="email" required>
                    </div>
                </div>
                <div class="col-xs-6">
                    <label for="line">Line ID</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="line" id="line" required>
                    </div>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-xs-12">
                    <label for="note">หมายเหตุ</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="note" id="note">
                    </div>
                </div>
            </div>
            <br>
            <br>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-9 col-centered">
            <div class="jumbotron">
                <center><h3>ข้าพเจ้ายินยอมและปฏิบัติตามกฎระเบียบข้อบังคับของ ชมรมแมวแห่งประเทศไทย</h3></center>
                <hr class="m-y-2">
                <p>
                    <div class="checkbox">
                        250 บาท/ปี
                        <label>
                            <input type="checkbox" required>
                            เงินสด
                        </label>
                    </div>
                    <div class="checkbox">
                        250 บาท/ปี
                        <label>
                            <input type="checkbox" required>
                            โอนเข้าบัญชี นางสุนันท์ โซวประเสริฐสุข กสิกรไทย 261-2-13432-7 พุทธมณฑลสาย 4
                        </label>
                    </div>
                </p>
                <center><h5>สามารถตรวจสอบรายชื่อฟาร์มได้ที่เว็บ www.cfct-cat.com เมนูดาวน์โหลดแบบฟอร์ม</h5></center>
            </div>

            <input type="hidden" name="form" value="member">
            <center>
                <button class="btn btn-primary" type="submit">ลงทะเบียน</button>
                <a class='btn btn-primary' href='index.php' role='button'>กลับหน้าหลัก</a>
            </center>
        </div>
    </div>
</form>

<script type="text/javascript">

$(document).ready(function() {
    $(':input').each(function () {
        var alert;
        if ($(this).attr("id") == "card_id") {
            alert = "ตัวเลข 13 หลัก";
        }
        else if ($(this).attr("id") == "phone") {
            alert = "ตัวเลข 10 หลัก";
        }
        else if ($(this).attr("id") == "name_en") {
            alert = "ตัวอักษรภาษาอังกฤษเท่านั้น";
        }
        else {
            alert = "โปรดกรอกข้อมูล";
        }

        this.oninvalid = function(e) {
            if (!e.target.validity.valid) {
                e.target.setCustomValidity(alert);
            }
            else {
                e.target.setCustomValidity("");
            }
        };
        this.onchange = function(e) {
            e.target.setCustomValidity("");
        }
    });
});

</script>
